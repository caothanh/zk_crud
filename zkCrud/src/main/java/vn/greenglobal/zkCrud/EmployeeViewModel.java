package vn.greenglobal.zkCrud;

import java.util.HashMap;
import java.util.List;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.lang.Strings;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Window;

import vn.greenglobal.zkCrud.entity.Employee;
import vn.greenglobal.zkCrud.entity.Log;
import vn.greenglobal.zkCrud.services.EmployeeService;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class EmployeeViewModel {

	@WireVariable
	private EmployeeService employeeService;
	private ListModelList<Employee> employeeListModel;
	private String name;
	private String position;
	private String company;
	private Employee selectEmployee;
	
	public Employee getSelectEmployee() {
		return selectEmployee;
	}

	public void setSelectEmployee(Employee selectEmployee) {
		this.selectEmployee = selectEmployee;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	@Init
	public void init() {
		List<Employee> em = employeeService.getEmployee();
		employeeListModel = new ListModelList<Employee>(em);
		System.out.println("list" + em);
	}
	
	public ListModel<Employee> getEmployeeListModel(){
		System.out.println("list1 " + employeeListModel);
		return employeeListModel;
	}
	
	@SuppressWarnings("unchecked")
	@Command
	@NotifyChange("employeeListModel")
	public void addEmployee(@BindingParam("cmp") Window x) {
		if (Strings.isBlank(name) && Strings.isBlank(position) && Strings.isBlank(company)) {
			return;
		}
		
		Employee employees = new Employee(name, position, company);
		System.out.println("luu : " + employees.getName()+" ," + employees.getPosition()+" ," + employees.getCompany()+" ," + employees.getId());
		employees = employeeService.addEmployee(employees);
		((List<Employee>) employeeListModel).add(employees);
		x.detach();
		BindUtils.postGlobalCommand(null, null, "refreshadd", null);
	}
	
	@GlobalCommand
	@NotifyChange("employeeListModel")
	public void refreshadd() {
		init();
	}

	@Command
	public void deleteEmployee(@BindingParam("employee") Employee employee) {
		employeeService.deleteEmployee(employee);
		employeeListModel.remove(employee);
	}
	
	@Command
	public void editEmployee(@BindingParam("employee") Employee employee) {
		name = employee.getName();
		position = employee.getPosition();
		company = employee.getCompany();
		selectEmployee = employee;
		
		final HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("selectEmployee", selectEmployee);
		final Window win = (Window) Executions.createComponents("/layout/edit-template.zul", null, map);
		win.setMinimizable(true);
		win.doModal();
	}

	@GlobalCommand
    @NotifyChange({"selectEmployee"})
	public void updateEmployee(@BindingParam("selectEmployee") Employee employee) {
		this.selectEmployee = selectEmployee;
		employeeService.updateEmployee(employee);
	}
	
}
