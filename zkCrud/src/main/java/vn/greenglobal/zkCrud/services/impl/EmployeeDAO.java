package vn.greenglobal.zkCrud.services.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.transaction.annotation.Transactional;

import vn.greenglobal.zkCrud.entity.Employee;

import org.springframework.stereotype.Repository;

@Repository
public class EmployeeDAO {

	@PersistenceContext
	private EntityManager manager;
	
	@Transactional(readOnly = true)
	public List<Employee> queryAll(){
		
		Query query =  manager.createQuery("SELECT e FROM Employee e");
		
		List<Employee> result = query.getResultList();
		
		return result;
	}
	
	@Transactional(readOnly = true)
	public Employee get(int id) {
		return manager.find(Employee.class, id);
	}
	
	@Transactional(readOnly = true)
	public Employee saveEmployee(Employee employee) {
		manager.persist(employee);
		manager.flush();
		
		return employee;
	}
	
	@Transactional(readOnly = true)
	public void deleteEmployee(Employee employee) {
		Employee employees = get(employee.getId());
		if (employees != null) {
			manager.remove(employees);
		}
	}
	
	@Transactional(readOnly = true)
	public void updateEmployee(Employee employee) {
		manager.merge(employee);
		manager.flush();
	}

}
