package vn.greenglobal.zkCrud.services;

import java.util.List;

import vn.greenglobal.zkCrud.entity.Employee;

public interface EmployeeService {

	Employee addEmployee(Employee employees);
	
	List<Employee> getEmployee();
	
	void deleteEmployee(Employee employees);
	
	void updateEmployee(Employee employees);
}
