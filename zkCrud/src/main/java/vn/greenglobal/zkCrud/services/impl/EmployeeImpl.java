package vn.greenglobal.zkCrud.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import vn.greenglobal.zkCrud.entity.Employee;
import vn.greenglobal.zkCrud.services.EmployeeService;

@Service("employeeService")
@Scope(value = "singleton", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class EmployeeImpl implements EmployeeService{

	@Autowired
	EmployeeDAO dao;
	
	@Override
	public Employee addEmployee(Employee employees) {
		return dao.saveEmployee(employees);
	}

	@Override
	public List<Employee> getEmployee() {
		return dao.queryAll();
	}

	@Override
	public void deleteEmployee(Employee employees) {
		dao.deleteEmployee(employees);
	}

	@Override
	public void updateEmployee(Employee employees) {
		dao.updateEmployee(employees);
	}

	
}
