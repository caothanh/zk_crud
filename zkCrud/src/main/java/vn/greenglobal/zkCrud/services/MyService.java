package vn.greenglobal.zkCrud.services;

import java.util.List;

import vn.greenglobal.zkCrud.entity.Log;

public interface MyService {

	Log addLog(Log log);

	List<Log> getLogs();

	void deleteLog(Log log);
}
