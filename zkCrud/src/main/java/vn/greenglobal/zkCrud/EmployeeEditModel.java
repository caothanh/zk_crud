package vn.greenglobal.zkCrud;

import java.util.HashMap;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Window;

import vn.greenglobal.zkCrud.entity.Employee;
import vn.greenglobal.zkCrud.services.EmployeeService;

public class EmployeeEditModel {

	@Wire("#winedit")
	private Window win;
	
	private String name;
	private String position;
	private String company;
	
	private Employee selectEmployee;
	private EmployeeService employeeService;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public Employee getSelectEmployee() {
		return selectEmployee;
	}
	public void setSelectEmployee(Employee selectEmployee) {
		this.selectEmployee = selectEmployee;
	}
	
	@Init
	public void init(@ContextParam(ContextType.VIEW) Component view, @ExecutionArgParam("selectEmployee") Employee em) {
		Selectors.wireComponents(view, this, false);
		this.selectEmployee = em;
	}

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Command
    public void save() {
    	Map map = new HashMap();
    	map.put("selectEmployee", this.selectEmployee);
    	BindUtils.postGlobalCommand(null, null, "updateEmployee", map);
    	win.detach();
    }
    
    @Command
    public void closeThis() {
        win.detach();
    }
}
